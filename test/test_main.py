import pytest
import allure


@allure.feature("Проверка работы 1")
@allure.story("Пчатает ли функция удачно")
def test_print_hi():
    with allure.step("Начало проверки"):
        assert True
    with allure.step("Удачная проверка"):
        assert 1 == 1
    with allure.step("Окончание проверки"):
        assert True


@allure.feature("Проверка работы 2")
@allure.story("Пчатает ли функция неудачно")
def test_print_hi_2():
    with allure.step("Неудачная проверка"):
        assert 1 == 2
